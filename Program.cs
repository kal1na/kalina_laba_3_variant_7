﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Lab_3_Kyrylo
{
    abstract class Bank
    {
        public int Money { get; set; }
        public string Name { get; set; }
        public Bank(int money,string name)
        {
            this.Money = money;
            this.Name = name;
        }
        public abstract void Info();
    }
    class RailwayStation : Bank
    {
        List<Client> ClientList = new List<Client>();
        List<Flight> FlightList = new List<Flight>();
        public RailwayStation(string name, int money) : base(money, name)
        {
            this.Money = 0;
        }
        public void AddClient(string name)
        {
            ClientList.Add(new Client(name, 0));
        }
        public void AddFlight(string name, int money)
        {
            FlightList.Add(new Flight(name, money));
        }
        public void ClientInfo()
        {
            int bank = 0;
            foreach(Client cli in ClientList)
            {
                cli.Info();
                bank += cli.Money;
            }
            Console.WriteLine($"All bank: {bank}");
        }
        public void FlightInfo()
        {
            foreach (Flight cal in FlightList)
            {
                cal.Info();
            }
        }
        public void BuyFlight()
        {
            try{
            ClientInfo();
            Console.WriteLine("Choose client: ");
            int clientindex = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" ");
            FlightInfo();
            Console.WriteLine("Choose your Flight: ");
            int Flightindex = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" ");
            ClientList[clientindex - 1].AddFlight(FlightList[Flightindex - 1].Name,FlightList[Flightindex - 1].Money);
            }
            catch
            {
                Console.WriteLine("Sorry something went wrong");
            }
        }
        public override void Info()
        {
            ClientInfo();
            Console.WriteLine("Enter client name: ");
            string FlightName = Console.ReadLine();
            foreach(Client cli in ClientList)
            {
                if(cli.Name == FlightName)
                {
                    foreach(string i in cli.Flights)
                    {
                        Console.WriteLine("Flight name: " + i);
                    }
                }
            }
        }
    }
    sealed class Client : Bank
    {
        public static int ID { get; set; }
        public int id { get; set; }
        public List<string> Flights = new List<string>();
        public Client(string name, int money) : base(money, name)
        {
            this.Money = 0;
            this.id = ID++;

        }
        public void AddFlight(string Flightname, int money)
        {
            Flights.Add(Flightname);
            Money += money;
        }
        public override void Info()
        {
            Console.WriteLine(id+1 + " : " + "Name: " + Name + " | Money has been spend: " + Money);
        }
    }
    class Flight : Bank
    {
        public static int ID { get; set; }
        public int id { get; set; }
        public Flight(string name, int money) : base(money, name)
        {
            this.id = ID++;
        }
        public override void Info()
        {
            Console.WriteLine(id+1 + " : "+"Name: " + Name + " | Cost: " + Money);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            bool end = true;
            int button = 0;
            RailwayStation Gek = new RailwayStation("Gek", 0);
            while(end)
            {
                Console.WriteLine("1.Add client"); // Ввести id клиента чтобы выбрать
                Console.WriteLine("2.Add Flight"); // Ввести id рейса чтобы выбрать
                Console.WriteLine("3.Client info"); // Покажет информацию о клиентах
                Console.WriteLine("4.Search by client name"); // Ввести имя(string) клиента чтобы выбрать
                Console.WriteLine("5.Order Flight"); //Ввести id клиента чтобы выбрать затем ввести id рейса чтобы выбрать
                Console.WriteLine("6.Press red button"); // Я НЕ ЗНАЮ ЧТО ПРОИЗОЙДЕТ 
                Console.WriteLine("7.Exit"); // Выйти из программы

                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        Console.WriteLine("Enter name: ");
                        string ClientName = Console.ReadLine();
                        Gek.AddClient(ClientName);
                        break;

                    case 2:
                        Console.WriteLine("Enter name: ");
                        string FlightName = Console.ReadLine();
                        Console.WriteLine("Enter cost: ");
                        int FlightCost = Convert.ToInt32(Console.ReadLine());
                        Gek.AddFlight(FlightName, FlightCost);
                        break;

                    case 3:
                        Gek.ClientInfo();
                        break;

                    case 4:
                        Gek.Info();
                        break;

                    case 5:
                        Gek.BuyFlight();
                        break;
                    case 6:
                        RedButton(ref button);
                        break;
                    case 7:
                        end = false;
                        break;
                }
            }
        }
        static void RedButton(ref int button)
        {
            button++;
            Console.WriteLine($"Значение переменной param в методе testMethod = {button}");
        }
    }
}